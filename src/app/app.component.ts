import { Component, OnInit, ViewChild, ElementRef, OnChanges } from '@angular/core';
import { AppService } from './app.service';
import { ArrayType } from '@angular/compiler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Angular Pagination';

  localData: any;

  pageSize: any;

  data: any;

  tableHeadings: string[];

  pages: any;

  selectedPageSize: number = 10;

  selectedPage: any = 1;

  currentPage: number = 1;
  totalPages: number;
  showAll: boolean = false;

  constructor(private appService: AppService) { }

  ngOnInit(): void {
    this.appService.getLocalData().subscribe(res => {
      if (Boolean(res)) {
        this.localData = res;
        this.data = res;
        this.pageSize = this.localData.length;
        this.tableHeadings = Object.keys(res[0]);
        this.setTotalPages();
        this.selectPage(this.currentPage);
      }
    });
  }

  setTotalPages() {
    this.totalPages = Math.floor((this.localData.length / this.selectedPageSize)) + (this.localData.length % this.selectedPageSize == 0 ? 0 : 1);
  }

  setTableData(selectedPage) {
    this.data = this.localData.slice(
      this.selectedPageSize * (selectedPage - 1),
      this.selectedPageSize * (selectedPage - 1) +
      +this.selectedPageSize
    );
  }

  changePageSize(pageSize: any) {
    if (pageSize == -1) {
      this.showAll = true;
      pageSize = this.localData.length;
    }
    this.selectedPageSize = pageSize;
    this.setTotalPages();
    this.selectPage(this.currentPage);
  }

  selectPage(selectedPage) {
    this.selectedPage = selectedPage;
    this.setTableData(selectedPage);
  }


  onClickButton(row) {
    alert('Selected ID = ' + row.id + ' and status = ' + row.status);
  }


  createPageNumber() {
    let leftSlot = 4, rightSlot = 3, leftPage, rightPage, openSlot = 0;
    if (this.currentPage - leftSlot > 1) {
      leftPage = this.currentPage - leftSlot;
    } else {
      openSlot = leftSlot - (this.currentPage - 2);
      leftPage = 2;
    }
    rightSlot += openSlot;
    if (rightSlot + this.currentPage < this.totalPages) {
      rightPage = this.currentPage + rightSlot;
    } else {
      openSlot = rightSlot + this.currentPage - this.totalPages + 1;
      rightPage = this.totalPages - 1;
    }
    if (leftPage > 2) {
      if (leftPage - openSlot > 2) {
        leftPage = leftPage - openSlot;
      } else {
        leftPage = 1;
      }
    }

    return Array.from(Array(rightPage - leftPage + 1), (_, i) => leftPage + i);
  }

  pageJump(page: number): void {
    this.currentPage = page;
    this.selectedPage = page;
    this.data = this.localData.slice(
      this.selectedPageSize * (page - 1),
      this.selectedPageSize * (page - 1) +
      +this.selectedPageSize
    );
  }

  pageDecrement(num: number): void {
    num = num || 1;
    if (this.currentPage - num >= 1) {
      this.currentPage = this.currentPage - num;
    } else {
      this.currentPage = 1;
    }
    this.pageJump(this.currentPage);
  }

  pageIncrement(num: number): void {
    num = num || 1;
    if (this.currentPage + num <= this.totalPages) {
      this.currentPage = this.currentPage + num;
    } else {
      this.currentPage = this.totalPages;
    }
    this.pageJump(this.currentPage);
  }
}
